# libp2p demo
Establishes a bidirectional link between two computers using the libp2p toolchain.

## Setup
Run `./bin/create-identities.js` to generate peer identities. It isn't necessary for libp2p, but it makes local testing much easier when the peer IDs (and consequently the address) doesn't keep changing.

Run `./src/listener.js` and `./src/dialer.js <peer-address>`.
