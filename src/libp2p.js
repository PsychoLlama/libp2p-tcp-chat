const Libp2p = require('libp2p');
const TCP = require('libp2p-tcp');
const { NOISE } = require('libp2p-noise');
const MPLEX = require('libp2p-mplex');
const MDNS = require('libp2p-mdns');

module.exports = async function init(options) {
  const node = await Libp2p.create({
    modules: {
      transport: [TCP],
      connEncryption: [NOISE],
      streamMuxer: [MPLEX],
      peerDiscovery: [MDNS],
    },
    ...options,
  });

  async function stop() {
    console.log('Terminating...');
    await node.stop();
    process.exit(0);
  }

  process.on('SIGTERM', stop);
  process.on('SIGINT', stop);

  return node;
}
