#!/usr/bin/env node
const multiaddr = require('multiaddr');
const Peer = require('peer-id');
const { stdinToStream, streamToConsole } = require('./stream');
const createPeer = require('./libp2p');

async function main() {
  const id = require('./identities/dialer.json');
  const node = await createPeer({
    peerId: await Peer.createFromJSON(id),
  });

  node.on('peer:discovery', async (peer) => {
    console.log('Connected.');
    const { stream } = await node.dialProtocol(peer, '/chat');
    stdinToStream(stream);
    streamToConsole(stream);
  });

  await node.start();
}

main();
