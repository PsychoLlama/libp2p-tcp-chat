#!/usr/bin/env node
const { stdinToStream, streamToConsole } = require('./stream');
const Peer = require('peer-id');
const createPeer = require('./libp2p');

async function main() {
  const id = require('./identities/listener');
  const node = await createPeer({
    peerId: await Peer.createFromJSON(id),
    addresses: {
      listen: ['/ip4/0.0.0.0/tcp/0'],
    },
  });

  node.connectionManager.on('peer:connect', (connection) => {
    console.log('Connection:', connection.remotePeer.toB58String());
  });

  await node.handle('/chat', ({ stream }) => {
    stdinToStream(stream);
    streamToConsole(stream);
  });

  await node.start();

  console.info('Listening:')
  node.multiaddrs.forEach(address => {
    console.info(`- ${address.toString()}/p2p/${node.peerId.toB58String()}`)
  });
}

main();
