#!/usr/bin/env node
const PeerId = require('peer-id');
const fs = require('fs/promises');
const path = require('path');

async function main() {
  const [listenerId, dialerId] = await Promise.all([
    PeerId.create(),
    PeerId.create(),
  ]);

  await fs.mkdir(fpath('../src/identities'), { recursive: true });
  await Promise.all([
    save(listenerId, '../src/identities/listener.json'),
    save(dialerId, '../src/identities/dialer.json'),
  ]);
}

function fpath(relative) {
  return path.resolve(`${__dirname}/${relative}`);
}

async function save(id, relativePath) {
  const serialized = JSON.stringify(id, null, 2) + '\n';
  await fs.writeFile(fpath(relativePath), serialized);

  return id;
}

main();
